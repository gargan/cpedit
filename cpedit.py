#!/usr/bin/python3
"""
insert something here
"""
import sys
import cairo
from PIL import Image
import math
import copy
import numpy
from scipy import interpolate
from typing import Any, List, Optional, Tuple, Dict, Union

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk, Gdk

import course_manager
import course_list


app_name = 'CPEdit'
app_ver = '0.1'


class Colors:
    WAYPOINT_LINE =            (0,       0,       0.5)

    ANGLE_INDICATOR =          (1,       1,       1)
    ANGLE_INDICATOR_SELECTED = (0,       0,       0)

    WAYPOINT_START =           (0,       0.75,    0)
    WAYPOINT_NORMAL =          (0,       0,       0.75)
    WAYPOINT_END =             (0.75,    0,       0)

    WAYPOINT_TURN_START =      (1,       0.625,   0.25)
    WAYPOINT_TURN_END =        (1,       0.25,    0.625)

    WAYPOINT_SELECTED_MAIN =   (1,       1,       1)
    WAYPOINT_SELECTED_OTHER =  (1,       1,       0)

    WAYPOINT_RING_INNER = WAYPOINT_START
    WAYPOINT_RING_OUTER = WAYPOINT_END


class CPEditApp(Gtk.Application):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        GLib.set_application_name(app_name)
        Gtk.IconTheme.get_default().prepend_search_path('icons')

        self.window: Optional[MainWindow] = None

    def do_startup(self) -> None:
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new('about', None)
        action.connect('activate', self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.on_quit)
        self.add_action(action)

        action = Gio.SimpleAction.new('open', None)
        action.connect('activate', self.on_open)
        self.add_action(action)

        action = Gio.SimpleAction.new('load_map', None)
        action.connect('activate', self.on_load_map)
        self.add_action(action)

        action = Gio.SimpleAction.new('save', None)
        action.connect('activate', self.on_save)
        self.add_action(action)

        str_type = GLib.Variant.new_string('').get_type()

        action = Gio.SimpleAction.new('align', str_type)
        action.connect('activate', self.on_align)
        self.add_action(action)

        action = Gio.SimpleAction.new('recalc_angles', None)
        action.connect('activate', self.on_recalc_angles)
        self.add_action(action)

        action = Gio.SimpleAction.new('distribute', str_type)
        action.connect('activate', self.on_distribute)
        self.add_action(action)

        builder = Gtk.Builder.new_from_file('app_menu.xml')
        self.set_menubar(builder.get_object('menubar'))

    def do_activate(self) -> None:
        # We only allow a single window and raise any existing ones
        if not self.window:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.window = MainWindow(application=self, title=app_name)

        self.window.present()

    # def do_command_line(self, command_line):
    #     options = command_line.get_options_dict()
    #
    #     # convert GVariantDict -> GVariant -> dict
    #     options = options.end().unpack()
    #
    #     self.activate()
    #     return 0

    def on_about(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        about_dialog: Gtk.AboutDialog = Gtk.AboutDialog.new()
        about_dialog.set_transient_for(self.window)

        about_dialog.set_copyright('Copyright \u00A9 2018 John Troxler')
        # about_dialog.set_authors(['John Troxler'])
        about_dialog.set_version(app_ver)

        about_dialog.connect('response', self.on_close)
        about_dialog.run()

        return True

    def on_close(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        action.destroy()

        return True

    def on_quit(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        self.quit()

        return True

    def on_open(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        if self.window:
            dialog: Gtk.FileChooserNative = Gtk.FileChooserNative.new('Open File', self.window, Gtk.FileChooserAction.OPEN,
                                                                      '_Open', '_Cancel')

            filter_xml = Gtk.FileFilter.new()
            filter_xml.set_name('XML files')
            filter_xml.add_mime_type('text/xml')
            dialog.add_filter(filter_xml)

            response = dialog.run()

            if response == Gtk.ResponseType.ACCEPT:
                self.window.load_xml(dialog.get_filename())

        return True

    def on_save(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        if self.window:
            self.window.save_xml()

        return True

    def on_load_map(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        if self.window:
            dialog: Gtk.FileChooserNative = Gtk.FileChooserNative.new('Open File', self.window, Gtk.FileChooserAction.OPEN,
                                                                      '_Open', '_Cancel')

            filter_dds = Gtk.FileFilter.new()
            filter_dds.set_name('DDS files')
            filter_dds.add_pattern('*.dds')
            dialog.add_filter(filter_dds)

            filter_png = Gtk.FileFilter.new()
            filter_png.set_name('PNG files')
            filter_png.add_mime_type('image/png')
            dialog.add_filter(filter_png)

            response = dialog.run()

            if response == Gtk.ResponseType.ACCEPT:
                self.window.load_map(dialog.get_filename())

        return True

    def on_align(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        if self.window and param:
            if param.get_string() == 'H':
                self.window.align_objects(Gtk.Orientation.HORIZONTAL)
            elif param.get_string() == 'V':
                self.window.align_objects(Gtk.Orientation.VERTICAL)

        return True

    def on_recalc_angles(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        if self.window:
            self.window.recalc_angles()

        return True

    def on_distribute(self, action: Gio.SimpleAction, param: Optional[GLib.Variant]) -> bool:
        if self.window and param:
            if param.get_string() == 'H':
                self.window.distribute_objects(Gtk.Orientation.HORIZONTAL)
            elif param.get_string() == 'V':
                self.window.distribute_objects(Gtk.Orientation.VERTICAL)

        return True


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        self.background_image_surface: Optional[cairo.ImageSurface] = None
        self.zoom: float = 0
        self.pan_mouse_origin: Optional[Tuple[float, float]] = None
        self.move_mouse_origin: Optional[Tuple[float, float]] = None
        self.waypoint_selection: List[course_manager.Waypoint] = []
        self.waypoint_selection_backup: Dict[int, Tuple[float, float]] = {}
        self.clicked_waypoint: Optional[Tuple[course_manager.Waypoint, bool]] = None
        self.select_box = (0, 0, 0, 0)
        self.mouse_button_1 = False
        self.mouse_button_3 = False
        self.mouse_move = False

        self.coursemanager: course_manager.CourseManager = None

        # This is deprecated, but I don't know an easier way yet
        self.set_default_size(0.8*self.get_screen().get_height(), 0.6*self.get_screen().get_height())

        # vertical box to hold toolbar, main window, and status bar
        vert_box: Gtk.Box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.add(vert_box)

        # toolbar
        vert_box.pack_start(self.create_toolbar(), False, False, 0)

        # main window
        main_grid = Gtk.Grid.new()
        vert_box.pack_start(main_grid, True, True, 0)

        self.drawing_area = self.create_drawing_area()
        main_grid.attach(self.drawing_area, 0, 0, 1, 2)

        self.vscroll, self.hscroll = self.create_scrollbars()
        main_grid.attach(self.vscroll, 1, 0, 1, 2)
        main_grid.attach(self.hscroll, 0, 2, 1, 1)

        self.treeview: course_list.CourseTreeView = course_list.CourseTreeView(self)
        main_grid.attach(self.treeview, 2, 0, 1, 1)

        data_grid, self.data_entry = self.create_waypoint_data_widgets()
        main_grid.attach(data_grid, 2, 1, 1, 1)

        self.show_all()

        # default background is empty image 2048x2048
        self.background_image_surface = cairo.ImageSurface(cairo.Format.RGB16_565, 2048, 2048)
        ctx = cairo.Context(self.background_image_surface)
        ctx.set_source_rgb(0.5, 0.5, 0.5)
        ctx.rectangle(0, 0, 2048, 2048)
        ctx.fill()

        scale = max(self.drawing_area.get_allocated_width() / 2048.0,
                    self.drawing_area.get_allocated_height() / 2048.0)

        self.set_scale(scale)

        self.vscroll.get_adjustment().configure(value=0,
                                                lower=-1024,
                                                upper=1024,
                                                step_increment=16,
                                                page_increment=self.drawing_area.get_allocated_height() / scale,
                                                page_size=self.drawing_area.get_allocated_height() / scale)

        self.hscroll.get_adjustment().configure(value=0,
                                                lower=-1024,
                                                upper=1024,
                                                step_increment=16,
                                                page_increment=self.drawing_area.get_allocated_width() / scale,
                                                page_size=self.drawing_area.get_allocated_width() / scale)

        self.drawing_area.queue_draw()

    def create_toolbutton(self, icon_name: str, icon_size: int, action_name: str, action_target: Optional[str], tooltip: str) -> Gtk.ToolButton:
        toolbar_button = Gtk.ToolButton.new(Gtk.Image.new_from_icon_name(icon_name, icon_size))

        toolbar_button.set_action_name(action_name)

        if action_target:
            toolbar_button.set_action_target_value(GLib.Variant.new_string(action_target))

        toolbar_button.set_tooltip_text(tooltip)

        return toolbar_button

    def create_toolbar(self) -> Gtk.Toolbar:
        toolbar = Gtk.Toolbar.new()
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)

        toolbar_button = self.create_toolbutton('halign-symbolic', Gtk.IconSize.LARGE_TOOLBAR, 'app.align', 'H', 'Align Horizontally')
        toolbar.insert(toolbar_button, -1)

        toolbar_button = self.create_toolbutton('valign-symbolic', Gtk.IconSize.LARGE_TOOLBAR, 'app.align', 'V', 'Align Vertically')
        toolbar.insert(toolbar_button, -1)

        toolbar_button = self.create_toolbutton('vdistribute-symbolic', Gtk.IconSize.LARGE_TOOLBAR, 'app.distribute', 'V', 'Distribute Vertically')
        toolbar.insert(toolbar_button, -1)

        toolbar_button = self.create_toolbutton('hdistribute-symbolic', Gtk.IconSize.LARGE_TOOLBAR, 'app.distribute', 'H', 'Distribute Horizontally')
        toolbar.insert(toolbar_button, -1)

        toolbar_button = self.create_toolbutton('recalc-angles-symbolic', Gtk.IconSize.LARGE_TOOLBAR, 'app.recalc_angles', None, 'Recalculate Angles')
        toolbar.insert(toolbar_button, -1)

        return toolbar

    def create_drawing_area(self) -> Gtk.DrawingArea:
        drawing_area = Gtk.DrawingArea.new()

        drawing_area.set_hexpand(True)
        drawing_area.set_can_focus(True)

        drawing_area.connect('draw', self.on_drawing_area_draw)
        drawing_area.connect('configure-event', self.on_drawing_area_configure_event)
        drawing_area.connect('scroll-event', self.on_drawing_area_scroll_event)
        drawing_area.connect('button-press-event', self.on_drawing_area_mouse_events)
        drawing_area.connect('button-release-event', self.on_drawing_area_mouse_events)
        drawing_area.connect('motion-notify-event', self.on_drawing_area_mouse_events)
        drawing_area.connect('key-press-event', self.on_drawing_area_key_events)

        #self.drawing_area.add_events(Gdk.EventMask.SCROLL_MASK | Gdk.EventMask.SMOOTH_SCROLL_MASK)
        drawing_area.add_events(Gdk.EventMask.SCROLL_MASK | Gdk.EventMask.BUTTON_PRESS_MASK |
                                Gdk.EventMask.BUTTON_RELEASE_MASK | Gdk.EventMask.BUTTON_MOTION_MASK)

        return drawing_area

    def create_scrollbars(self) -> Tuple[Gtk.Scrollbar, Gtk.Scrollbar]:
        v_scrollbar = Gtk.Scrollbar.new(Gtk.Orientation.VERTICAL)
        h_scrollbar = Gtk.Scrollbar.new(Gtk.Orientation.HORIZONTAL)

        v_scrollbar.get_adjustment().configure(value=0, lower=0, upper=1, step_increment=0, page_increment=0,
                                               page_size=1)
        h_scrollbar.get_adjustment().configure(value=0, lower=0, upper=1, step_increment=0, page_increment=0,
                                               page_size=1)

        v_scrollbar.connect('value-changed', self.on_vscroll_value_changed)
        h_scrollbar.connect('value-changed', self.on_hscroll_value_changed)

        return v_scrollbar, h_scrollbar

    def create_waypoint_data_widgets(self) -> Tuple[Gtk.Grid, dict]:
        data_grid = Gtk.Grid.new()

        data_entry = {
            'pos.x': {'label': Gtk.Label.new('Position.x'),   'entry': Gtk.Entry.new(), 'handler_id': None, 'blocked': False},
            'pos.y': {'label': Gtk.Label.new('Position.y'),   'entry': Gtk.Entry.new(), 'handler_id': None, 'blocked': False},
            'angle': {'label': Gtk.Label.new('Angle (Deg.)'), 'entry': Gtk.Entry.new(), 'handler_id': None, 'blocked': False},
            'speed': {'label': Gtk.Label.new('Speed'),        'entry': Gtk.Entry.new(), 'handler_id': None, 'blocked': False},
            'lane':  {'label': Gtk.Label.new('Lane'),         'entry': Gtk.Entry.new(), 'handler_id': None, 'blocked': False},

            'wait':      {'label': Gtk.Label.new('Wait'),       'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},
            'unload':    {'label': Gtk.Label.new('Unload'),     'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},
            'reverse':   {'label': Gtk.Label.new('Reverse'),    'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},
            'crossing':  {'label': Gtk.Label.new('Crossing'),   'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},
            'generated': {'label': Gtk.Label.new('Generated'),  'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},
            'turnstart': {'label': Gtk.Label.new('Turn Start'), 'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},
            'turnend':   {'label': Gtk.Label.new('Turn End'),   'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},

            'ridgemarker': {
                'label': Gtk.Label.new('Ridge Marker'),
                'box': Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0),
                'radiogroup': [
                    {'radiobutton': Gtk.RadioButton().new_with_label(None, 'None'),  'handler_id': None, 'blocked': False},
                    {'radiobutton': Gtk.RadioButton().new_with_label(None, 'Left'),  'handler_id': None, 'blocked': False},
                    {'radiobutton': Gtk.RadioButton().new_with_label(None, 'right'), 'handler_id': None, 'blocked': False}
                ]
            },

            'headlandheightforturn': {'label': Gtk.Label.new('Headland Height\nfor Turn = 0'), 'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},
            'isconnectingtrack': {'label': Gtk.Label.new('Connecting Track'), 'checkbox': Gtk.CheckButton.new(), 'handler_id': None, 'blocked': False},

            'radius': {'label': Gtk.Label.new('Radius'), 'entry': Gtk.Entry.new(), 'handler_id': None, 'blocked': False}
        }

        for index, key in enumerate(data_entry):
            data_entry[key]['label'].set_halign(Gtk.Align.END)
            data_entry[key]['label'].set_margin_start(5)
            data_entry[key]['label'].set_margin_end(5)
            data_entry[key]['label'].set_justify(Gtk.Justification.RIGHT)

            data_grid.attach(data_entry[key]['label'], 0, index, 1, 1)

            if 'entry' in data_entry[key]:
                entry = data_entry[key]['entry']

                entry.set_sensitive(False)
                entry.set_text('')

                data_entry[key]['handler_id'] = entry.connect('changed', self.on_data_entry_changed)
                entry.connect('focus-out-event', self.on_data_entry_focus_out_event)

                data_grid.attach(entry, 1, index, 1, 1)
            elif 'checkbox' in data_entry[key]:
                checkbox = data_entry[key]['checkbox']

                checkbox.set_sensitive(False)

                data_entry[key]['handler_id'] = checkbox.connect('toggled', self.on_data_checkbutton_toggled)

                data_grid.attach(checkbox, 1, index, 1, 1)
            elif 'radiogroup' in data_entry[key]:
                lastbutton = None

                for radiobutton_dict in data_entry[key]['radiogroup']:
                    radiobutton = radiobutton_dict['radiobutton']

                    if lastbutton:
                        radiobutton.join_group(lastbutton)

                    radiobutton.set_sensitive(False)

                    radiobutton_dict['handler_id'] = radiobutton.connect('toggled', self.on_data_radiobutton_toggled)

                    data_entry[key]['box'].pack_start(radiobutton, False, False, 0)

                    lastbutton = radiobutton

                data_grid.attach(data_entry[key]['box'], 1, index, 1, 1)

        return data_grid, data_entry

    def on_data_radiobutton_toggled(self, radiobutton: Gtk.RadioButton) -> bool:
        if self.waypoint_selection and radiobutton.get_active():
            selected_waypoint = self.waypoint_selection[0]

            for index, radiobutton_dict in enumerate(self.data_entry['ridgemarker']['radiogroup']):
                if radiobutton == radiobutton_dict['radiobutton']:
                    selected_waypoint.ridge_marker = index
                    break

        return True

    def on_data_checkbutton_toggled(self, checkbutton: Gtk.CheckButton) -> bool:
        if self.waypoint_selection:
            selected_waypoint = self.waypoint_selection[0]

            if checkbutton == self.data_entry['wait']['checkbox']:
                selected_waypoint.wait = checkbutton.get_active()
            elif checkbutton == self.data_entry['unload']['checkbox']:
                selected_waypoint.unload = checkbutton.get_active()
            elif checkbutton == self.data_entry['reverse']['checkbox']:
                selected_waypoint.reverse = checkbutton.get_active()
            elif checkbutton == self.data_entry['crossing']['checkbox']:
                selected_waypoint.crossing = checkbutton.get_active()
            elif checkbutton == self.data_entry['generated']['checkbox']:
                selected_waypoint.generated = checkbutton.get_active()
            elif checkbutton == self.data_entry['turnstart']['checkbox']:
                selected_waypoint.turn_start = checkbutton.get_active()
            elif checkbutton == self.data_entry['turnend']['checkbox']:
                selected_waypoint.turn_end = checkbutton.get_active()
            elif checkbutton == self.data_entry['headlandheightforturn']['checkbox']:
                selected_waypoint.headland_height_for_turn_is_zero = checkbutton.get_active()
            elif checkbutton == self.data_entry['isconnectingtrack']['checkbox']:
                selected_waypoint.is_connecting_track = checkbutton.get_active()

            self.drawing_area.queue_draw()

        return True

    def on_data_entry_focus_out_event(self, entry: Gtk.Entry, event: Gdk.EventFocus) -> bool:
        self.drawing_area.queue_draw()

        return Gdk.EVENT_PROPAGATE

    def load_map(self, map_file: str) -> None:
        if map_file:
            im: Image = Image.open(map_file)

            if im.mode != 'RGBA':
                if im.mode == 'RGB':
                    im.putalpha(255)
                else:
                    im = im.convert('RGBA')

            self.background_image_surface = cairo.ImageSurface.create_for_data(memoryview(bytearray(im.tobytes('raw', 'BGRa'))), cairo.FORMAT_ARGB32, im.width, im.height)

            scale = self.get_scale()

            self.vscroll.get_adjustment().configure(value=self.vscroll.get_value(),
                                                    lower=-self.background_image_surface.get_height()/2,
                                                    upper=self.background_image_surface.get_height()/2,
                                                    step_increment=16,
                                                    page_increment=self.drawing_area.get_allocated_height() / scale,
                                                    page_size=self.drawing_area.get_allocated_height() / scale)

            self.hscroll.get_adjustment().configure(value=self.hscroll.get_value(),
                                                    lower=-self.background_image_surface.get_width()/2,
                                                    upper=self.background_image_surface.get_width()/2,
                                                    step_increment=16,
                                                    page_increment=self.drawing_area.get_allocated_width() / scale,
                                                    page_size=self.drawing_area.get_allocated_width() / scale)

            self.drawing_area.queue_draw()

    def on_data_entry_changed(self, entry: Gtk.Entry) -> bool:
        if self.waypoint_selection:
            selected_waypoint = self.waypoint_selection[0]

            if entry == self.data_entry['pos.x']['entry']:
                selected_waypoint.x = entry.get_text()
            elif entry == self.data_entry['pos.y']['entry']:
                selected_waypoint.y = entry.get_text()
            elif entry == self.data_entry['angle']['entry']:
                selected_waypoint.angle = entry.get_text()
            elif entry == self.data_entry['speed']['entry']:
                selected_waypoint.speed = entry.get_text()
            elif entry == self.data_entry['lane']['entry']:
                selected_waypoint.lane = entry.get_text()
            elif entry == self.data_entry['radius']['entry']:
                selected_waypoint.radius = entry.get_text()

            self.drawing_area.queue_draw()

        return True

    def on_drawing_area_key_events(self, drawing_area: Gtk.DrawingArea, event: Gdk.EventKey) -> bool:
        if event.keyval == Gdk.KEY_Escape:
            if self.mouse_button_1 and self.clicked_waypoint and self.move_mouse_origin:
                for waypoint in self.waypoint_selection:
                    if waypoint.uid in self.waypoint_selection_backup:
                        old_waypoint_x, old_waypoint_y = self.waypoint_selection_backup[waypoint.uid]

                        waypoint.x = old_waypoint_x
                        waypoint.y = old_waypoint_y

                self.mouse_button_1 = False
                self.clicked_waypoint = None
                self.waypoint_selection_backup = {}
                self.move_mouse_origin = None

                self.drawing_area.queue_draw()

        return True

    def on_drawing_area_scroll_event(self, drawing_area: Gtk.DrawingArea, event: Gdk.EventScroll) -> bool:
        if self.background_image_surface:
            scale = self.get_scale()
            mouse_pos = (event.x / scale + self.hscroll.get_value(), event.y / scale + self.vscroll.get_value())

            if event.direction == Gdk.ScrollDirection.UP:
                scale = self.get_scale_from_zoom(round(self.zoom + 1))
            elif event.direction == Gdk.ScrollDirection.DOWN:
                scale = self.get_scale_from_zoom(round(self.zoom - 1))

            # check to make sure we haven't zoomed out too much
            if self.drawing_area.get_allocated_width() > scale * self.background_image_surface.get_width() or self.drawing_area.get_allocated_height() > scale * self.background_image_surface.get_height():
                # zoomed out too far
                scale = max(self.drawing_area.get_allocated_width() / self.background_image_surface.get_width(),
                            self.drawing_area.get_allocated_height() / self.background_image_surface.get_height())

            self.set_scale(scale)

            lower_x = -self.background_image_surface.get_width() / 2.0
            lower_y = -self.background_image_surface.get_height() / 2.0

            upper_x = self.background_image_surface.get_width() / 2.0
            upper_y = self.background_image_surface.get_height() / 2.0

            origin = (min(upper_x - self.drawing_area.get_allocated_width() / scale, max(lower_x, mouse_pos[0] - event.x / scale)),
                      min(upper_y - self.drawing_area.get_allocated_height() / scale, max(lower_y, mouse_pos[1] - event.y / scale)))

            self.update_scrollbars(origin)

            self.drawing_area.queue_draw()

        return True

    def on_drawing_area_mouse_events(self, drawing_area: Gtk.DrawingArea, event: Gdk.EventMotion) -> bool:
        if self.background_image_surface:
            scale = self.get_scale()

            if event.get_event_type() == Gdk.EventType.BUTTON_PRESS:
                if not self.mouse_button_1 and not self.mouse_button_3:
                    if event.button == 1:
                        self.mouse_button_1 = True
                        self.mouse_button_3 = False
                        self.mouse_move = False

                        # force focus to the drawing area on left click
                        self.drawing_area.grab_focus()

                        # determine if we've clicked a waypoint or not
                        self.clicked_waypoint = None

                        mouse_x = self.hscroll.get_value() + event.x / scale
                        mouse_y = self.vscroll.get_value() + event.y / scale

                        for course in self.treeview.get_enabled_courses():
                            if self.clicked_waypoint:
                                break

                            for waypoint in course.waypoints:
                                delta_x = waypoint.x - mouse_x
                                delta_y = waypoint.y - mouse_y
                                dist = math.sqrt(delta_x * delta_x + delta_y * delta_y)

                                if dist <= 1:
                                    if waypoint in self.waypoint_selection:
                                        self.clicked_waypoint = (waypoint, True)
                                    else:
                                        self.clicked_waypoint = (waypoint, False)
                                    break

                        if self.clicked_waypoint:
                            # determine if this waypoint is already in the current selection
                            if not self.clicked_waypoint[1]:
                                if event.state & Gdk.ModifierType.CONTROL_MASK:
                                    self.waypoint_selection.append(self.clicked_waypoint[0])
                                else:
                                    self.waypoint_selection = [self.clicked_waypoint[0]]
                            elif not (event.state & Gdk.ModifierType.CONTROL_MASK) and event.state & Gdk.ModifierType.SHIFT_MASK:
                                # move clicked waypoint to top
                                self.waypoint_selection.remove(self.clicked_waypoint[0])
                                self.waypoint_selection.insert(0, self.clicked_waypoint[0])

                            # backup current selection
                            if (event.state & Gdk.ModifierType.CONTROL_MASK) or not (event.state & Gdk.ModifierType.SHIFT_MASK):
                                self.move_mouse_origin = (mouse_x, mouse_y)
                                self.waypoint_selection_backup = {}
                                for waypoint in self.waypoint_selection:
                                    self.waypoint_selection_backup[waypoint.uid] = (waypoint.x, waypoint.y)
                        else:
                            # clicked on open space
                            self.select_box = (self.hscroll.get_value() + event.x / scale, self.vscroll.get_value() + event.y / scale, 0, 0)

                            # if no ctrl modifier then deselect all waypoints
                            if not (event.state & Gdk.ModifierType.CONTROL_MASK):
                                self.waypoint_selection = []
                    elif event.button == 3:
                        self.mouse_button_1 = False
                        self.mouse_button_3 = True

                        self.pan_mouse_origin = (self.hscroll.get_value() + event.x / scale, self.vscroll.get_value() + event.y / scale)
            elif event.get_event_type() == Gdk.EventType.MOTION_NOTIFY:
                if self.mouse_button_1 and event.state & Gdk.ModifierType.BUTTON1_MASK:
                    self.mouse_move = True

                    if self.clicked_waypoint:
                        # move selection
                        if self.move_mouse_origin:
                            mouse_x = self.hscroll.get_value() + event.x / scale
                            mouse_y = self.vscroll.get_value() + event.y / scale

                            delta_x = mouse_x - self.move_mouse_origin[0]
                            delta_y = mouse_y - self.move_mouse_origin[1]

                            for waypoint in self.waypoint_selection:
                                if waypoint.uid in self.waypoint_selection_backup:
                                    old_waypoint_x, old_waypoint_y = self.waypoint_selection_backup[waypoint.uid]

                                    waypoint.x = old_waypoint_x + delta_x
                                    waypoint.y = old_waypoint_y + delta_y
                    else:
                        # draw selection box
                        self.select_box = (self.select_box[0], self.select_box[1],
                                           self.hscroll.get_value() + event.x / scale - self.select_box[0],
                                           self.vscroll.get_value() + event.y / scale - self.select_box[1])
                elif self.mouse_button_3 and event.state & Gdk.ModifierType.BUTTON3_MASK:
                    if self.pan_mouse_origin:
                        self.update_scrollbars((self.pan_mouse_origin[0] - event.x / scale,
                                                self.pan_mouse_origin[1] - event.y / scale))
            elif event.get_event_type() == Gdk.EventType.BUTTON_RELEASE:
                if self.mouse_button_1 and event.button == 1:
                    if self.mouse_move:
                        # mouse move before button release
                        self.mouse_move = False

                        # if self.clicked_waypoint:
                        #     # clicked waypoint
                        #     print('end-move-select')
                        # else:
                        if not self.clicked_waypoint:
                            # open space
                            select_left = min(self.select_box[0], self.select_box[0] + self.select_box[2])
                            select_right = max(self.select_box[0], self.select_box[0] + self.select_box[2])
                            select_top = min(self.select_box[1], self.select_box[1] + self.select_box[3])
                            select_bottom = max(self.select_box[1], self.select_box[1] + self.select_box[3])

                            for course in self.treeview.get_enabled_courses():
                                for waypoint in course.waypoints:
                                    if select_left <= waypoint.x <= select_right and select_top <= waypoint.y <= select_bottom:
                                        # add to selection
                                        if waypoint not in self.waypoint_selection:
                                            self.waypoint_selection.append(waypoint)
                                        else:
                                            self.waypoint_selection.remove(waypoint)
                    else:
                        # single click, no mouse movement
                        if self.clicked_waypoint and self.clicked_waypoint[1]:
                            if event.state & Gdk.ModifierType.CONTROL_MASK:
                                self.waypoint_selection.remove(self.clicked_waypoint[0])
                            elif not (event.state & Gdk.ModifierType.SHIFT_MASK):
                                self.waypoint_selection = [self.clicked_waypoint[0]]

                    self.mouse_button_1 = False
                    self.clicked_waypoint = None
                    self.select_box = (0, 0, 0, 0)

                    self.waypoint_selection_backup = {}
                    self.move_mouse_origin = None
                elif self.mouse_button_3 and event.button == 3:
                    self.mouse_button_3 = False
                    self.pan_mouse_origin = None

            self.drawing_area.queue_draw()

        return True

    def on_drawing_area_draw(self, drawing_area: Gtk.DrawingArea, context: cairo.Context) -> bool:
        if self.background_image_surface:
            # context.save()

            # clip extents: (left, top, right, bottom)
            # context.translate(tx, ty) -> (left - tx, top - ty, right - tx, bottom - ty
            # context.scale(sx, sy) -> (left/sx, top/sy, right/sx, bottom/sy
            #
            # if translate (tx,ty) first then scale (sx, sy):
            # ((left - tx)/sx, (top - ty)/sy, (right - tx)/sx, (bottom - ty)/sy)
            #
            # if scale (sx, sy) first then translate (tx, ty):
            # (left/sx - tx, top/sy - ty, right/sx - tx, bottom/sy - ty)

            scale = self.get_scale()
            context.scale(scale, scale)
            context.translate(-(self.background_image_surface.get_width() / 2.0 + self.hscroll.get_value()), -(self.background_image_surface.get_height() / 2.0 + self.vscroll.get_value()))

            # paint the background image
            context.set_source_surface(self.background_image_surface)
            context.get_source().set_filter(cairo.Filter.FAST)
            context.paint()

            context.translate(self.background_image_surface.get_width() / 2.0, self.background_image_surface.get_height() / 2.0)

            # paint the courses
            for course in self.treeview.get_enabled_courses():
                if course.waypoints:
                    waypoint_prev = course.waypoints[0]

                    for waypoint in course.waypoints[1:]:
                        # draw a line from the previous point to the current point
                        self.draw_waypoint_line(context, waypoint_prev, waypoint)

                        # draw the previous point, but not if it's the main selected point.  We will draw that last.
                        if waypoint_prev in self.waypoint_selection and waypoint_prev != self.waypoint_selection[0]:
                            self.draw_waypoint(context, waypoint_prev, selected=True)
                        else:
                            if waypoint_prev == course.waypoints[0]:
                                self.draw_waypoint(context, waypoint_prev, Colors.WAYPOINT_START)
                            else:
                                self.draw_waypoint(context, waypoint_prev)

                        waypoint_prev = waypoint

                    # Draw the last point, but not if it's the main selected point.  We will draw that last.
                    if waypoint_prev in self.waypoint_selection and waypoint_prev != self.waypoint_selection[0]:
                        self.draw_waypoint(context, waypoint_prev, selected=True)
                    else:
                        self.draw_waypoint(context, waypoint_prev, Colors.WAYPOINT_END)

            # Draw the main selected point if it exists and update the data entries
            if self.waypoint_selection:
                self.update_waypoint_data_entry(self.waypoint_selection[0])
                self.draw_waypoint(context, self.waypoint_selection[0], selected=True, is_main=True)
            else:
                self.update_waypoint_data_entry(None)

            # draw the select box if necessary
            if self.select_box[2] != 0 or self.select_box[3] != 0:
                context.set_line_width(1.0 / scale)
                context.rectangle(*self.select_box)
                context.set_source_rgb(1, 1, 1)
                context.stroke()

            # context.restore()

        return True

    def draw_waypoint_line(self, context: cairo.Context, waypoint_from: course_manager.Waypoint, waypoint_to: course_manager.Waypoint) -> None:
        half_line_width = 0.125
        use_stroke = False

        scale = self.get_scale()
        waypoint_from_x = waypoint_from.x       # Optimization
        waypoint_from_y = waypoint_from.y       # Optimization

        if half_line_width * scale < 0.5:
            half_line_width = 0.5 / scale
            use_stroke = True

        context.set_source_rgb(*Colors.WAYPOINT_LINE)

        if use_stroke:
            # This is only used at small scales, so the linecap being round isn't necessary
            context.set_line_width(2*half_line_width)
            # context.set_line_cap(cairo.LineCap.ROUND)
            context.move_to(waypoint_from_x, waypoint_from_y)
            context.line_to(waypoint_to.x, waypoint_to.y)

            context.stroke()
        else:
            # I normally would use context.stroke() to draw this line, but I've noticed some really funny/weird behavior
            # near the edges of the clipping region while zoomed in.  It seems like cairo can't calculate the stroke
            # outlines properly if the lines move outside of the clipping region when we are zoomed in sufficiently.
            # I don't know how to fix but drawing the line as a filled polygon works properly.
            delta_y = waypoint_to.y - waypoint_from_y
            delta_x = waypoint_to.x - waypoint_from_x
            line_length = math.sqrt(delta_x * delta_x + delta_y * delta_y)

            context.save()

            context.translate(waypoint_from_x, waypoint_from_y)
            context.rotate(math.atan2(delta_y, delta_x))

            context.arc(0, 0, half_line_width, math.pi/2, 3*math.pi/2)
            context.line_to(line_length, -half_line_width)
            context.arc(line_length, 0, half_line_width, -math.pi/2, math.pi/2)
            context.close_path()

            context.fill()

            context.restore()

    def draw_waypoint(self, context: cairo.Context, waypoint: course_manager.Waypoint, color: Tuple[float, float, float] = None, selected: bool = False, is_main: bool = False) -> None:
        if not selected:
            inner_radius = 0.5
            outer_radius = 1.0
        else:
            inner_radius = 0.45
            outer_radius = 1.05

        draw_angle_indicator = True

        scale = self.get_scale()
        waypoint_x = waypoint.x         # Optimization
        waypoint_y = waypoint.y         # Optimization

        if (outer_radius - inner_radius) * scale < 1.0:
            if not selected:
                # outer_over_inner = outer_radius / inner_radius
                #
                # inner_radius = (1.0 / (outer_over_inner - 1)) / scale
                # outer_radius = inner_radius + 1.0 / scale

                inner_radius = 1.0 / scale
                outer_radius = 2.0 / scale
            else:
                # outer_over_inner = outer_radius / inner_radius
                #
                # inner_radius = (1.0 / (outer_over_inner - 1)) / scale
                # outer_radius = inner_radius + 1.0 / scale

                inner_radius = 0.75 / scale
                outer_radius = 1.75 / scale

            draw_angle_indicator = False

        # draw point
        context.arc(waypoint_x, waypoint_y, outer_radius, 0, 2 * math.pi)
        context.new_sub_path()
        context.arc_negative(waypoint_x, waypoint_y, inner_radius, 0, -2 * math.pi)

        if not selected:
            if not color:
                if waypoint.turn_start:
                    context.set_source_rgb(*Colors.WAYPOINT_TURN_START)
                elif waypoint.turn_end:
                    context.set_source_rgb(*Colors.WAYPOINT_TURN_END)
                else:
                    context.set_source_rgb(*Colors.WAYPOINT_NORMAL)
            else:
                context.set_source_rgb(*color)
        else:
            if is_main:
                context.set_source_rgb(*Colors.WAYPOINT_SELECTED_MAIN)
            else:
                context.set_source_rgb(*Colors.WAYPOINT_SELECTED_OTHER)

        context.fill()

        if is_main:
            context.set_line_width(1.0 / self.get_scale())

            context.arc(waypoint_x, waypoint_y, 2.5, 0, 2 * math.pi)
            context.set_source_rgb(*Colors.WAYPOINT_RING_INNER)
            context.stroke()

            context.arc(waypoint_x, waypoint_y, 5, 0, 2 * math.pi)
            context.set_source_rgb(*Colors.WAYPOINT_RING_OUTER)
            context.stroke()

        # draw angle indicator
        if draw_angle_indicator:
            context.save()

            context.translate(waypoint_x, waypoint_y)
            context.rotate(math.radians(-(waypoint.angle - 90)))
            if not selected:
                # 0.2618 ~= math.radians(15)
                context.set_source_rgb(*Colors.ANGLE_INDICATOR)
                context.arc(0, 0, inner_radius, -0.2618, 0.2618)
            else:
                # 0.3491 ~= math.radians(20)
                context.set_source_rgb(*Colors.ANGLE_INDICATOR_SELECTED)
                context.arc(0, 0, inner_radius, -0.3491, 0.3491)

            context.line_to(outer_radius, 0)
            context.close_path()
            context.fill()

            context.restore()

    def get_scale_from_zoom(self, zoom: float) -> float:
        return math.exp(0.095 * zoom)

    def get_zoom_from_scale(self, scale: float) -> float:
        return math.log(scale) / 0.095

    def get_scale(self) -> float:
        return self.get_scale_from_zoom(self.zoom)

    def set_scale(self, scale: float) -> None:
        self.zoom = self.get_zoom_from_scale(scale)

    def on_drawing_area_configure_event(self, drawing_area: Gtk.DrawingArea, event: Gdk.EventConfigure) -> bool:
        if self.background_image_surface:
            scale = self.get_scale()

            if self.drawing_area.get_allocated_width() > scale * self.background_image_surface.get_width() or self.drawing_area.get_allocated_height() > scale * self.background_image_surface.get_height():
                # zoomed out too far
                scale = max(self.drawing_area.get_allocated_width() / self.background_image_surface.get_width(),
                            self.drawing_area.get_allocated_height() / self.background_image_surface.get_height())

            self.set_scale(scale)

            upper_x = self.background_image_surface.get_width() / 2.0
            upper_y = self.background_image_surface.get_height() / 2.0

            origin = (min(upper_x - self.drawing_area.get_allocated_width() / scale, self.hscroll.get_value()),
                      min(upper_y - self.drawing_area.get_allocated_height() / scale, self.vscroll.get_value()))

            self.update_scrollbars(origin)

        return True

    def on_vscroll_value_changed(self, scrollbar: Gtk.Scrollbar) -> bool:
        self.drawing_area.queue_draw()

        return True

    def on_hscroll_value_changed(self, scrollbar: Gtk.Scrollbar) -> bool:
        self.drawing_area.queue_draw()

        return True

    def update_scrollbars(self, origin: Tuple[float, float]) -> None:
        if self.background_image_surface:
            scale = self.get_scale()

            hscroll_adjustment: Gtk.Adjustment = self.hscroll.get_adjustment()
            vscroll_adjustment: Gtk.Adjustment = self.vscroll.get_adjustment()

            hscroll_page_size = self.drawing_area.get_allocated_width() / scale
            vscroll_page_size = self.drawing_area.get_allocated_height() / scale

            hscroll_adjustment.set_page_size(hscroll_page_size)
            hscroll_adjustment.set_page_increment(hscroll_page_size)

            vscroll_adjustment.set_page_size(vscroll_page_size)
            vscroll_adjustment.set_page_increment(vscroll_page_size)

            hscroll_adjustment.set_value(origin[0])
            vscroll_adjustment.set_value(origin[1])

    def update_waypoint_data_entry(self, waypoint: course_manager.Waypoint) -> None:
        # block all entries from emitting changed signals
        for key in self.data_entry:
            if 'entry' in self.data_entry[key]:
                # only block entries that don't have focus
                if not self.data_entry[key]['entry'].has_focus():
                    self.data_entry[key]['entry'].handler_block(self.data_entry[key]['handler_id'])
                    self.data_entry[key]['blocked'] = True
            elif 'checkbox' in self.data_entry[key]:
                self.data_entry[key]['checkbox'].handler_block(self.data_entry[key]['handler_id'])
                self.data_entry[key]['blocked'] = True
            elif 'radiogroup' in self.data_entry[key]:
                for radiobutton_dict in self.data_entry[key]['radiogroup']:
                    radiobutton_dict['radiobutton'].handler_block(radiobutton_dict['handler_id'])
                    radiobutton_dict['blocked'] = True

        if not waypoint:
            # no selected main point so disable entries
            for key in self.data_entry:
                if 'entry' in self.data_entry[key]:
                    self.data_entry[key]['entry'].set_sensitive(False)
                    self.data_entry[key]['entry'].set_text('')
                elif 'checkbox' in self.data_entry[key]:
                    self.data_entry[key]['checkbox'].set_sensitive(False)
                    self.data_entry[key]['checkbox'].set_active(False)
                elif 'radiogroup' in self.data_entry[key]:
                    for radiobutton_dict in self.data_entry[key]['radiogroup']:
                        radiobutton_dict['radiobutton'].set_sensitive(False)

                    self.data_entry[key]['radiogroup'][0]['radiobutton'].set_active(True)
        else:
            entry_value = {
                'pos.x': waypoint.x,
                'pos.y': waypoint.y,
                'angle': waypoint.angle,
                'speed': waypoint.speed,
                'lane':  waypoint.lane,
                'wait': waypoint.wait,
                'unload': waypoint.unload,
                'reverse': waypoint.reverse,
                'crossing': waypoint.crossing,
                'generated': waypoint.generated,
                'turnstart': waypoint.turn_start,
                'turnend': waypoint.turn_end,
                'ridgemarker': waypoint.ridge_marker,
                'headlandheightforturn': waypoint.headland_height_for_turn_is_zero,
                'isconnectingtrack': waypoint.is_connecting_track,
                'radius': waypoint.radius
            }

            for key in self.data_entry:
                if 'entry' in self.data_entry[key]:
                    if not self.data_entry[key]['entry'].has_focus():
                        self.data_entry[key]['entry'].set_text(f'{entry_value[key]}')

                    self.data_entry[key]['entry'].set_sensitive(True)
                elif 'checkbox' in self.data_entry[key]:
                    self.data_entry[key]['checkbox'].set_active(entry_value[key])
                    self.data_entry[key]['checkbox'].set_sensitive(True)
                elif 'radiogroup' in self.data_entry[key]:
                    self.data_entry[key]['radiogroup'][entry_value[key]]['radiobutton'].set_active(True)

                    for radiobutton_dict in self.data_entry[key]['radiogroup']:
                        radiobutton_dict['radiobutton'].set_sensitive(True)

        # allow blocked entries to emit changed signals
        for key in self.data_entry:
            if 'entry' in self.data_entry[key]:
                if self.data_entry[key]['blocked']:
                    self.data_entry[key]['entry'].handler_unblock(self.data_entry[key]['handler_id'])
                    self.data_entry[key]['blocked'] = False
            elif 'checkbox' in self.data_entry[key]:
                if self.data_entry[key]['blocked']:
                    self.data_entry[key]['checkbox'].handler_unblock(self.data_entry[key]['handler_id'])
                    self.data_entry[key]['blocked'] = False
            elif 'radiogroup' in self.data_entry[key]:
                for radiobutton_dict in self.data_entry[key]['radiogroup']:
                    if radiobutton_dict['blocked']:
                        radiobutton_dict['radiobutton'].handler_unblock(radiobutton_dict['handler_id'])
                        radiobutton_dict['blocked'] = False

    def align_objects(self, direction: Gtk.Orientation) -> None:
        if len(self.waypoint_selection) > 1:
            primary_waypoint = self.waypoint_selection[0]

            for waypoint in self.waypoint_selection[1:]:
                if direction == Gtk.Orientation.HORIZONTAL:
                    waypoint.x = primary_waypoint.x
                elif direction == Gtk.Orientation.VERTICAL:
                    waypoint.y = primary_waypoint.y

            self.drawing_area.queue_draw()

    def process_segment(self, waypoint_list: List[course_manager.Waypoint]) -> None:
        if waypoint_list:
            # for now, I ignore turns...
            if not waypoint_list[0].turn_start:
                # check length
                if len(waypoint_list) == 1:
                    # not sure this is possible, but do nothing
                    pass
                elif len(waypoint_list) == 2:
                    # two points make a line, so angles are equal and pointing from the first to the second
                    waypoint_list[0].angle = waypoint_list[1].angle = math.degrees(math.atan2(-waypoint_list[1].y, waypoint_list[1].x)) + 90
                else:
                    np_x = numpy.array([waypoint.x for waypoint in waypoint_list])
                    np_y = numpy.array([waypoint.y for waypoint in waypoint_list])

                    try:
                        tck, u = interpolate.splprep([np_x, np_y], s=0)
                        xi, yi = interpolate.splev(u, tck, der=1)
                    except ValueError:
                        # If the spline interpolation fails, fall back to simple approximation
                        # all points except last point
                        xi = np_x[2:] - np_x[:-2]
                        yi = np_y[2:] - np_y[:-2]

                        # first point points toward next
                        xi = numpy.insert(xi, 0, np_x[1] - np_x[0])
                        yi = numpy.insert(yi, 0, np_y[1] - np_y[0])

                        # last point points away from last
                        xi = numpy.append(xi, np_x[-1] - np_x[-2])
                        yi = numpy.append(yi, np_y[-1] - np_y[-2])

                    # update waypoint angles
                    for index, waypoint in enumerate(waypoint_list):
                        waypoint.angle = math.degrees(math.atan2(-yi[index], xi[index])) + 90

    def recalc_angles(self) -> None:
        # loop through all enabled courses
        for course in self.treeview.get_enabled_courses():
            waypoint_list = []

            for waypoint in course.waypoints:
                waypoint_list.append(waypoint)

                # check if this is the end of a segment
                if waypoint.turn_start or waypoint.turn_end:
                    self.process_segment(waypoint_list)

                    waypoint_list.clear()

            # process last segment
            if waypoint_list:
                self.process_segment(waypoint_list)

        self.drawing_area.queue_draw()

    def distribute_objects(self, direction: Gtk.Orientation) -> None:
        if len(self.waypoint_selection) > 1:
            if direction == Gtk.Orientation.HORIZONTAL:
                # change x
                x_list = numpy.array([waypoint.x for waypoint in self.waypoint_selection])
                sort_index_list = numpy.argsort(x_list)
                x_dist = numpy.linspace(numpy.amin(x_list), numpy.amax(x_list), len(self.waypoint_selection))

                for index, x_value in enumerate(x_dist):
                    self.waypoint_selection[sort_index_list[index]].x = x_value

            elif direction == Gtk.Orientation.VERTICAL:
                # change y
                y_list = numpy.array([waypoint.y for waypoint in self.waypoint_selection])
                sort_index_list = numpy.argsort(y_list)
                y_dist = numpy.linspace(numpy.amin(y_list), numpy.amax(y_list), len(self.waypoint_selection))

                for index, y_value in enumerate(y_dist):
                    self.waypoint_selection[sort_index_list[index]].y = y_value

            self.drawing_area.queue_draw()

    def load_xml(self, coursemanager_file: str) -> None:
        if coursemanager_file:
            try:
                self.coursemanager = course_manager.CourseManager(coursemanager_file)

                # clear the treeview
                self.treeview.clear()

                # get each course and insert into treeview, inserting parent folders as necessary
                for course_id, course in self.coursemanager.courses.items():
                    self.treeview.add_course(course)
            except RuntimeError as err:
                self.treeview.clear()
                self.coursemanager = None

                self.messagebox_error('Error loading CourseManager XML file.', str(err))

            self.drawing_area.queue_draw()

    def save_xml(self) -> None:
        if self.coursemanager:
            try:
                self.coursemanager.save()
            except RuntimeError as err:
                self.messagebox_error('Error saving CourseManager XML file.', str(err))

    def messagebox_error(self, message_text: str, secondary_text: Optional[str] = None) -> None:
        dialog: Gtk.MessageDialog = Gtk.MessageDialog(parent=self, flags=0, message_type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.CLOSE, text=message_text)
        if secondary_text:
            dialog.format_secondary_text('Click Close to certify that you have read me carefully.')

        box: Gtk.Box = dialog.get_message_area().get_parent()
        box.set_spacing(0)

        image: Gtk.Image = Gtk.Image.new_from_icon_name('dialog-error', Gtk.IconSize.DIALOG)
        image.set_valign(Gtk.Align.START)
        image.show()

        box.pack_start(image, False, False, 0)
        box.reorder_child(image, 0)

        dialog.show()

        space = round(dialog.get_message_area().get_children()[0].get_layout_offsets().x - (box.get_parent().get_allocated_width() - box.get_allocated_width()) / 2 - image.get_allocated_width())

        if space < 20:
            box.set_spacing(20 - space)

        dialog.run()
        dialog.destroy()


if __name__ == '__main__':
    app = CPEditApp(application_id='com.gmail.johntroxler.cpedit') #, flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE)
    sys.exit(app.run(sys.argv))
