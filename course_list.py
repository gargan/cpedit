from __future__ import annotations

from gi.repository import Gtk
from enum import Enum
from typing import Union, List, TYPE_CHECKING

if TYPE_CHECKING:
    import course_manager


# Typing aliases
TreeItemPathType = Union[Gtk.TreeIter, Gtk.TreePath, int, str]


class TreeItemState(Enum):
    UNCHECKED = 0
    CHECKED = 1
    INCONSISTENT = 2


class CourseTreeView(Gtk.TreeView):
    def __init__(self, parent: Gtk.ApplicationWindow) -> None:
        Gtk.TreeView.__init__(self)

        self.parent_window: Gtk.ApplicationWindow = parent

        self.treeview_store: Gtk.TreeStore = Gtk.TreeStore.new([str, bool, bool, str])
        self.set_model(self.treeview_store)

        self.set_headers_visible(False)
        self.get_selection().set_mode(Gtk.SelectionMode.NONE)
        self.set_vexpand(True)

        column = Gtk.TreeViewColumn.new()

        toggle_renderer = Gtk.CellRendererToggle.new()
        text_renderer = Gtk.CellRendererText.new()

        column.pack_start(toggle_renderer, False)
        column.pack_start(text_renderer, True)

        column.add_attribute(text_renderer, 'text', 0)
        column.add_attribute(toggle_renderer, 'active', 1)
        column.add_attribute(toggle_renderer, 'inconsistent', 2)

        toggle_renderer.connect('toggled', self.on_treeitem_toggled)

        self.append_column(column)

    def on_treeitem_toggled(self, widget: Gtk.CellRendererToggle, path: str) -> bool:
        state = self.get_treeitem_state(path)

        if state == TreeItemState.UNCHECKED or state == TreeItemState.INCONSISTENT:
            self.set_treeitem_state(path, TreeItemState.CHECKED, True)
            self.set_treeitem_parent(path, TreeItemState.CHECKED)
        else:
            self.set_treeitem_state(path, TreeItemState.UNCHECKED, True)
            self.set_treeitem_parent(path, TreeItemState.UNCHECKED)

        for course in self.get_disabled_courses():
            self.parent_window.waypoint_selection = [waypoint for waypoint in self.parent_window.waypoint_selection if waypoint not in course.waypoints]

        self.parent_window.drawing_area.queue_draw()

        return True

    def get_treeitem_state(self, path: TreeItemPathType) -> TreeItemState:
        active = self.treeview_store[path][1]
        inconsistent = self.treeview_store[path][2]

        if active:
            return TreeItemState.CHECKED
        elif inconsistent:
            return TreeItemState.INCONSISTENT
        else:
            return TreeItemState.UNCHECKED

    def set_treeitem_state(self, path: TreeItemPathType, state: TreeItemState, recursive: bool = False) -> None:
        # set current item
        if state == TreeItemState.CHECKED:
            self.treeview_store[path][1] = True
            self.treeview_store[path][2] = False
        elif state == TreeItemState.UNCHECKED:
            self.treeview_store[path][1] = False
            self.treeview_store[path][2] = False
        elif state == TreeItemState.INCONSISTENT:
            self.treeview_store[path][1] = False
            self.treeview_store[path][2] = True

        if recursive:
            if not isinstance(path, Gtk.TreeIter):
                path = self.treeview_store.get_iter(path)

            tree_iter = self.treeview_store.iter_children(path)

            while tree_iter:
                self.set_treeitem_state(tree_iter, state, True)

                tree_iter = self.treeview_store.iter_next(tree_iter)

    def set_treeitem_parent(self, path: TreeItemPathType, state: TreeItemState) -> None:
        # first set this item
        self.set_treeitem_state(path, state)

        # get item parent if exists
        if not isinstance(path, Gtk.TreeIter):
            path = self.treeview_store.get_iter(path)

        parent_iter = self.treeview_store.iter_parent(path)

        if parent_iter:
            if state == TreeItemState.INCONSISTENT:
                # We just set this item to inconsistent, so we only need to set the parent to inconsistent as well
                self.set_treeitem_parent(parent_iter, TreeItemState.INCONSISTENT)
            else:
                # check if all siblings are the same state as us
                child_iter = self.treeview_store.iter_children(parent_iter)
                new_state = state

                while child_iter:
                    if state != self.get_treeitem_state(child_iter):
                        # different states, make inconsistent
                        new_state = TreeItemState.INCONSISTENT
                        break

                    child_iter = self.treeview_store.iter_next(child_iter)

                self.set_treeitem_parent(parent_iter, new_state)

    def find_courses_by_state(self, iter: Gtk.TreeIter, state: TreeItemState) -> List[course_manager.Course]:
        check_list = []

        while iter:
            item_type, item_id = self.treeview_store[iter][3].split('_', 1)
            item_id = int(item_id)

            if item_type == 'course' and self.get_treeitem_state(iter) == state:
                if item_id in self.parent_window.coursemanager.courses:
                    check_list.append(self.parent_window.coursemanager.courses[item_id])

            child_iter = self.treeview_store.iter_children(iter)
            check_list.extend(self.find_courses_by_state(child_iter, state))
            iter = self.treeview_store.iter_next(iter)

        return check_list

    def get_enabled_courses(self) -> List[course_manager.Course]:
        root_iter = self.treeview_store.get_iter_first()

        return self.find_courses_by_state(root_iter, TreeItemState.CHECKED)

    def get_disabled_courses(self) -> List[course_manager.Course]:
        root_iter = self.treeview_store.get_iter_first()

        return self.find_courses_by_state(root_iter, TreeItemState.UNCHECKED)

    def clear(self) -> None:
        self.treeview_store.clear()

    def add_to_treelist(self, parent: Gtk.TreeIter, text: str, id_str: str) -> Gtk.TreeIter:
        item_type, item_id = id_str.split('_', 1)

        # insertion sort, folders before courses then by id
        child_iter = self.treeview_store.iter_children(parent)

        while child_iter:
            child_id_str = self.treeview_store.get_value(child_iter, 3)
            child_type, child_id = child_id_str.split('_', 1)

            # courses are larger than folders
            if (child_type == 'course' and item_type == 'folder') or (child_type == item_type and child_id > item_id):
                # this element is larger
                break

            child_iter = self.treeview_store.iter_next(child_iter)

        if child_iter:
            # found larger element, so insert before
            return self.treeview_store.insert_before(parent, child_iter, [text, True, False, id_str])
        else:
            # found no larger element, so append to end
            return self.treeview_store.append(parent, [text, True, False, id_str])

    def add_course(self, course: course_manager.Course) -> None:
        # start off at the root level
        parent_iter = None

        for parent_id in course.parent_id_list:
            # search for this id at the current level
            child_iter = self.treeview_store.iter_children(parent_iter)
            id_found = False

            while child_iter:
                if f'folder_{parent_id}' == self.treeview_store.get_value(child_iter, 3):
                    # parent id found
                    parent_iter = child_iter
                    id_found = True
                    break

                child_iter = self.treeview_store.iter_next(child_iter)

            if not id_found:
                # this parent was not found, so insert and move on
                if parent_id in self.parent_window.coursemanager.folders:
                    parent_iter = self.add_to_treelist(parent_iter, self.parent_window.coursemanager.folders[parent_id].name, f'folder_{parent_id}')
                else:
                    raise RuntimeError(f'Folder with id = "{parent_id}" not found.')

        # by this point parent_iter should be equal to the parent of the current course so we can now insert
        self.add_to_treelist(parent_iter, course.name, f'course_{course.id}')
