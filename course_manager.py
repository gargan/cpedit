import xml.etree.ElementTree as ET
import pathlib
import math
import time
import shutil
from typing import Any, Optional, Union, List, Dict


MAXBACKUPS = 10

# Type aliases
NumStrType = Union[str, int, float]


class XmlMixin:
    _element: ET.Element
    xml_modified: bool

    def set_int_bool_xml_attribute(self, key: str, value: bool) -> None:
        orig_value = self._element.get(key)

        if value:
            self._element.set(key, '1')
        elif key in self._element.keys():
            del self._element.attrib[key]

        if orig_value != self._element.get(key):
            self.xml_modified = True

    def set_bool_xml_attribute(self, key: str, value: bool) -> None:
        orig_value = self._element.get(key)

        if value:
            self._element.set(key, 'true')
        elif key in self._element.keys():
            del self._element.attrib[key]

        if orig_value != self._element.get(key):
            self.xml_modified = True

    def set_int_xml_attribute(self, key: str, value: int, set_cond: bool = True) -> None:
        orig_value = self._element.get(key)

        if set_cond:
            self._element.set(key, f'{round(value)}')
        elif key in self._element.keys():
            del self._element.attrib[key]

        if orig_value != self._element.get(key):
            self.xml_modified = True

    def format_float_str(self, value: float, precision: int) -> str:
        float_str = f'{value:.{precision}f}'.rstrip('0').rstrip('.')

        if float_str == '-0':
            float_str = '0'

        return float_str

    def set_float_xml_attribute(self, key: str, value: float, precision: int, set_cond: bool = True) -> None:
        # convert float to string then call string func
        self.set_string_xml_attribute(key, self.format_float_str(value, precision), set_cond)

    def set_string_xml_attribute(self, key: str, value: str, set_cond: bool = True) -> None:
        orig_value = self._element.get(key)

        if set_cond:
            self._element.set(key, value)
        elif key in self._element.keys():
            del self._element.attrib[key]

        if orig_value != self._element.get(key):
            self.xml_modified = True


class Waypoint(XmlMixin):
    @property
    def x(self) -> float:
        return self.__x

    @x.setter
    def x(self, value: NumStrType) -> None:
        try:
            self.__x = round(float(value), 2)
        except ValueError:
            pass

    @property
    def y(self) -> float:
        return self.__y

    @y.setter
    def y(self, value: NumStrType) -> None:
        try:
            self.__y = round(float(value), 2)
        except ValueError:
            pass

    @property
    def angle(self) -> float:
        return self.__angle

    @angle.setter
    def angle(self, value: NumStrType) -> None:
        # I prefer +180 degrees rather than -180 degrees so i'll force value to be within the interval (-180, 180].
        # This if-else is required because math.fmod(x,y) returns a result that is the same sign as x and whose
        # magnitude is less than abs(y). I use math.fmod() instead of % for the floating-point accuracy.
        #
        # proof:
        # ------
        # math.fmod(x, y) = x - n*y for some integer n such that sign(x - n*y) = sign(x) and abs(x - n*y) < abs(y). If
        # we assume y > 0 then n can be found in the following manner. If x >= 0 then n = floor(x/y) and if x < 0 then
        # n = ceil(x/y).
        #
        # for value <= 180:
        #   let x = 180 - value and y = 360. Then x >= 0 and so n = floor(x/y) = floor((180 - value)/360).
        #   new_value = 180 - math.fmod(180 - value, 360)
        #             = 180 - (180 - value - n*360)
        #             = value + n*360
        #   and n = floor((180-value)/360) implies that
        #   (180 - value)/360 - 1 < n <= (180 - value)/360
        #   -180 < value + n*360 <= 180
        #
        # for value > 180:
        #   let x = 180 - value and y = 360. Then x < 0 and so n = ceil(x/y) = ceil((180 - value)/360).
        #   new_value = -180 - math.fmod(180 - value, 360)
        #             = -180 - (180 - value - n*360)
        #             = value + (n-1)*360
        #   and n = ceil((180 - value)/360) implies that
        #   (180 - value)/360 < n < (180 - value)/360 + 1
        #   -180 < value + (n - 1)*360 < 180
        #
        # Thus in all cases, new_value = value + m*360 for some integer m and -180 < new_value <= 180
        try:
            value = float(value)
        except ValueError:
            pass
        else:
            if value <= 180.0:
                value = 180 - math.fmod(180 - value, 360)
            else:
                value = -180 - math.fmod(180 - value, 360)

            self.__angle = round(value, 2)

    @property
    def speed(self) -> int:
        return self.__speed

    @speed.setter
    def speed(self, value: NumStrType) -> None:
        try:
            self.__speed = round(float(value))
        except ValueError:
            pass

    # The rest of these can be None
    @property
    def wait(self) -> bool:
        return self.__wait

    @wait.setter
    def wait(self, value: Any) -> None:
        self.__wait = bool(value)

    @property
    def unload(self) -> bool:
        return self.__unload

    @unload.setter
    def unload(self, value: Any) -> None:
        self.__unload = bool(value)

    @property
    def reverse(self) -> bool:
        return self.__reverse

    @reverse.setter
    def reverse(self, value: Any) -> None:
        self.__reverse = bool(value)

    @property
    def crossing(self) -> bool:
        # return self.get_xml_int('crossing') == 1 or self.num == 1
        return self.__crossing

    @crossing.setter
    def crossing(self, value: Any) -> None:
        self.__crossing = bool(value)

    @property
    def generated(self) -> bool:
        # return self.get_xml_bool('generated')
        return self.__generated

    @generated.setter
    def generated(self, value: Any) -> None:
        # self.set_xml_bool('generated', value)
        self.__generated = bool(value)

    @property
    def lane(self) -> int:
        return self.__lane

    @lane.setter
    def lane(self, value: NumStrType) -> None:
        try:
            value = round(int(value))
        except ValueError:
            pass
        else:
            if value > 0:
                value = 0

            self.__lane = value

    @property
    def turn_start(self) -> bool:
        return self.__turn_start

    @turn_start.setter
    def turn_start(self, value: Any) -> None:
        self.__turn_start = bool(value)

    @property
    def turn_end(self) -> bool:
        return self.__turn_end

    @turn_end.setter
    def turn_end(self, value: Any) -> None:
        self.__turn_end = bool(value)

    @property
    def ridge_marker(self) -> int:
        return self.__ridge_marker

    # Possible values for ridge_marker: 0 - none, 1 - left, 2 - right
    @ridge_marker.setter
    def ridge_marker(self, value: NumStrType) -> None:
        try:
            value = int(value)
        except ValueError:
            pass
        else:
            if value == 0 or value == 1 or value == 2:
                self.__ridge_marker = value

    # currently possible values for headland_height_for_turn: 0 or None
    @property
    def headland_height_for_turn_is_zero(self) -> bool:
        return self.__headland_height_for_turn_is_zero

    @headland_height_for_turn_is_zero.setter
    def headland_height_for_turn_is_zero(self, value: Any) -> None:
        self.__headland_height_for_turn_is_zero = bool(value)

    @property
    def is_connecting_track(self) -> bool:
        return self.__is_connecting_track

    @is_connecting_track.setter
    def is_connecting_track(self, value: Any) -> None:
        self.__is_connecting_track = bool(value)

    @property
    def radius(self) -> float:
        return self.__radius

    @radius.setter
    def radius(self, value: NumStrType) -> None:
        try:
            value = float(value)
        except ValueError:
            pass
        else:
            if value < 0:
                value = 0

            self.__radius = round(value, 1)

    @property
    def uid(self) -> int:
        return self.__uid

    def __init__(self, element: ET.Element, num: int, course: 'Course'):
        self._element: ET.Element = element
        self.__course: 'Course' = course
        self.__uid: int = course.coursemanager.get_unique_id()

        self.num: int = num

        self.x, self.y = self.get_xml_attribute('pos')
        self.angle = self.get_xml_attribute('angle')
        self.speed = self.get_xml_attribute('speed')
        self.wait = self.get_xml_attribute('wait')
        self.unload = self.get_xml_attribute('unload')
        self.reverse = self.get_xml_attribute('rev')
        self.crossing = self.get_xml_attribute('crossing')
        self.generated = self.get_xml_attribute('generated')
        self.is_connecting_track = self.get_xml_attribute('isconnectingtrack')
        self.turn_start = self.get_xml_attribute('turnstart')
        self.turn_end = self.get_xml_attribute('turnend')
        self.headland_height_for_turn_is_zero = self.get_xml_attribute('headlandheightforturn')
        self.lane = self.get_xml_attribute('lane')
        self.ridge_marker = self.get_xml_attribute('ridgemarker')
        self.radius = self.get_xml_attribute('radius')

        self.xml_modified: bool = False

    def get_xml_attribute(self, attrib: str) -> Any:
        if attrib == 'pos':
            return tuple(self._element.get('pos').split(' ', 1))
        elif attrib == 'angle':
            return self._element.get('angle', 0)
        elif attrib == 'speed':
            speed = float(self._element.get('speed', 0))
            if math.ceil(speed) != speed:
                speed = math.ceil(speed * 3600)
            return speed
        elif attrib == 'wait':
            return self._element.get('wait') == '1'
        elif attrib == 'unload':
            return self._element.get('unload') == '1'
        elif attrib == 'rev':
            return self._element.get('rev') == '1'
        elif attrib == 'crossing':
            return self._element.get('crossing') == '1'
        elif attrib == 'generated':
            return self._element.get('generated', '').lower() == 'true'
        elif attrib == 'isconnectingtrack':
            return self._element.get('isconnectingtrack', '').lower() == 'true'
        elif attrib == 'turnstart':
            return self._element.get('turnstart') == '1'
        elif attrib == 'turnend':
            return self._element.get('turnend') == '1'
        elif attrib == 'headlandheightforturn':
            return self._element.get('headlandheightforturn') == '0'
        elif attrib == 'lane':
            return self._element.get('lane', 0)
        elif attrib == 'ridgemarker':
            return self._element.get('ridgemarker', 0)
        elif attrib == 'radius':
            return self._element.get('radius', 0)

        return None

    def update_xml(self, final: bool = True) -> None:
        # Required:
        #   pos:   [float float] = '{x:.2f} {y:.2f}'
        #   angle: float         = '{angle:.2f}'
        #   speed: int           = '{speed:d}'
        self.set_string_xml_attribute('pos', f'{self.format_float_str(self.x, 2)} {self.format_float_str(self.y, 2)}')
        self.set_float_xml_attribute('angle', self.angle, precision=2)
        self.set_int_xml_attribute('speed', self.speed)

        # Optional:
        #   wait:                     bool       = True: '1', False: None
        #   unload:                   bool       = True: '1', False: None
        #   reverse:                  bool       = True: '1', False: None
        #   crossing:                 bool       = True: '1', False: None
        #   generated:                bool       = True: 'true', False: None
        #   lane:                     int        = lane < 0: '{lane:d}', lane >= 0: None
        #   turn_start:               bool       = True: '1', False: None
        #   turn_end:                 bool       = True: '1', False: None
        #   ridge_marker:             int        = 0 < ridge_marker <= 2: '{ridge_marker:d}', ridge_marker == 0: None
        #   headland_height_for_turn: int/None   = headland_height_for_turn == 0: '0', headland_height_for_turn != 0: None
        #   is_connecting_track:      bool       = True: 'true', False: None
        #   radius:                   float      = radius > 0: '{radius:.1f}', radius <= 0: None
        self.set_int_bool_xml_attribute('wait', self.wait)
        self.set_int_bool_xml_attribute('unload', self.unload)
        self.set_int_bool_xml_attribute('rev', self.reverse)
        self.set_int_bool_xml_attribute('crossing', self.crossing)
        self.set_int_bool_xml_attribute('turnstart', self.turn_start)
        self.set_int_bool_xml_attribute('turnend', self.turn_end)

        self.set_bool_xml_attribute('generated', self.generated)
        self.set_bool_xml_attribute('isconnectingtrack', self.is_connecting_track)

        self.set_int_xml_attribute('lane', self.lane, set_cond=(self.lane < 0))
        self.set_int_xml_attribute('ridgemarker', self.ridge_marker, set_cond=(self.ridge_marker != 0))

        # Not sure how 'headlandheightforturn' should be set, so for now, only set to '0' or nothing
        self.set_int_xml_attribute('headlandheightforturn', 0, set_cond=self.headland_height_for_turn_is_zero)

        self.set_float_xml_attribute('radius', self.radius, precision=1, set_cond=(self.radius > 0))

        if final:
            self.xml_modified = False


class Course:
    @property
    def filename(self) -> str:
        return self.__filename

    @filename.setter
    def filename(self, value: str) -> None:
        self.__filename = value

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, value: str) -> None:
        self.__name = value

    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, value: NumStrType) -> None:
        try:
            value = int(value)
        except ValueError:
            pass
        else:
            if value >= 0:
                self.__id = value

    @property
    def parent(self) -> int:
        return self.__parent

    @parent.setter
    def parent(self, value: NumStrType) -> None:
        try:
            value = int(value)
        except ValueError:
            pass
        else:
            if value >= 0:
                self.__parent = value

    @property
    def is_used(self) -> bool:
        return self.__is_used

    @is_used.setter
    def is_used(self, value: Any) -> None:
        self.__is_used = bool(value)

    @property
    def work_width(self) -> Optional[float]:
        return self.__work_width

    @work_width.setter
    def work_width(self, value: Optional[NumStrType]) -> None:
        if value:
            try:
                value = float(value)
            except ValueError:
                pass
            else:
                self.__work_width: Optional[float] = value
        else:
            self.__work_width: Optional[float] = None

    @property
    def num_headland_lanes(self) -> Optional[int]:
        return self.__num_headland_lanes

    @num_headland_lanes.setter
    def num_headland_lanes(self, value: Optional[NumStrType]) -> None:
        if value:
            try:
                value = int(value)
            except ValueError:
                pass
            else:
                self.__num_headland_lanes: Optional[int] = value
        else:
            self.__num_headland_lanes: Optional[int] = None

    @property
    def headland_direction_cw(self) -> Optional[bool]:
        return self.__headland_direction_cw

    @headland_direction_cw.setter
    def headland_direction_cw(self, value: Optional[Any]) -> None:
        if value:
            self.__headland_direction_cw: Optional[bool] = bool(value)
        else:
            self.__headland_direction_cw: Optional[bool] = None

    @property
    def multi_tools(self) -> Optional[int]:
        return self.__multi_tools

    @multi_tools.setter
    def multi_tools(self, value: Optional[NumStrType]) -> None:
        if value:
            try:
                value = int(value)
            except ValueError:
                pass
            else:
                self.__multi_tools: Optional[int] = value
        else:
            self.__multi_tools: Optional[int] = None

    def __init__(self, slot: ET.Element, coursemanager: 'CourseManager'):
        self.__slot = slot
        self.coursemanager = coursemanager

        self.filename = self.get_xml_attribute('fileName')
        self.id = self.get_xml_attribute('id')
        self.name = self.get_xml_attribute('name')
        self.parent = self.get_xml_attribute('parent')
        self.is_used = self.get_xml_attribute('isUsed')

        # Don't know what to do with these yet
        self.work_width = self.get_xml_attribute('workWidth')
        self.num_headland_lanes = self.get_xml_attribute('numHeadlandLanes')
        self.headland_direction_cw = self.get_xml_attribute('headlandDirectionCW')
        self.multi_tools = self.get_xml_attribute('multiTools')

        # load course xml file
        self.file = str(pathlib.Path(pathlib.PurePath(self.coursemanager.file).parent, self.filename).resolve())
        self.xml_tree = ET.parse(self.file)
        self.root = self.xml_tree.getroot()

        # root should be tagged with course
        if self.root.tag != 'course':
            raise RuntimeError(f'{self.file} is not a valid course file.')

        # traverse parent_id_list
        self.parent_id_list: List[int] = []

        parent_id = self.parent
        while parent_id != 0:
            self.parent_id_list.insert(0, parent_id)

            # find the folder with this id and get its parent
            folder: Optional[ET.Element] = self.coursemanager.root.find(f"folders/folder[@id='{parent_id}']")

            if folder is not None:
                parent_id = int(folder.get('parent'))
            else:
                parent_id = 0

        # create waypoint list
        self.waypoints: List[Waypoint] = []

        waypoint_num = 1
        waypoint = self.root.find(f'waypoint{waypoint_num}')

        while waypoint is not None:
            self.waypoints.append(Waypoint(waypoint, waypoint_num, course=self))

            waypoint_num += 1
            waypoint = self.root.find(f'waypoint{waypoint_num}')

        self.num_waypoints = len(self.waypoints)
        self.xml_modified = False

    def get_xml_attribute(self, attrib: str) -> Any:
        if attrib == 'fileName':
            return self.__slot.get('fileName', '')
        elif attrib == 'id':
            return self.__slot.get('id', 0)
        elif attrib == 'name':
            return self.__slot.get('name', default=f'NO_NAME{self.id}')
        elif attrib == 'parent':
            return self.__slot.get('parent', 0)
        elif attrib == 'isUsed':
            return self.__slot.get('isUsed', default='').lower() == 'true'
        elif attrib == 'workWidth':
            return self.__slot.get('workWidth')
        elif attrib == 'numHeadlandLanes':
            return self.__slot.get('numHeadlandLanes')
        elif attrib == 'headlandDirectionCW':
            return self.__slot.get('headlandDirectionCW')
        elif attrib == 'multiTools':
            return self.__slot.get('multiTools')

        return None

    def update_xml(self, final: bool = True) -> None:
        # Required:
        #   is_used:  bool = True: 'true', False: 'false'
        #   filename: str  = filename
        #   id:       int  = '{id:d}'
        #   name:     str  = name
        #   parent:   int  = '{parent:d}'

        # Optional:
        #   work_width:            float = (work_width != false and work_width != nil): '{work_width:.6f}', else: None
        #   num_headland_lanes:    int   = (num_headland_lanes != false and num_headland_lanes != nil): '{num_headland_lanes:d}', else: None
        #   headland_direction_cw: bool  = headland_direction_cw != nil: headland_direction_cw, else: None
        #   multi_tools:           int   = multi_tools != nil: '{multi_tools:d}', else: None

        # self.set_int_bool_xml_attribute('wait', self.wait)
        # self.set_int_bool_xml_attribute('unload', self.unload)
        # self.set_int_bool_xml_attribute('rev', self.reverse)
        # self.set_int_bool_xml_attribute('crossing', self.crossing)
        # self.set_int_bool_xml_attribute('turnstart', self.turn_start)
        # self.set_int_bool_xml_attribute('turnend', self.turn_end)
        #
        # self.set_bool_xml_attribute('generated', self.generated)
        # self.set_bool_xml_attribute('isconnectingtrack', self.is_connecting_track)
        #
        # self.set_int_xml_attribute('lane', self.lane, set_cond=(self.lane < 0))
        # self.set_int_xml_attribute('ridgemarker', self.ridge_marker, set_cond=(self.ridge_marker != 0))
        #
        # # Not sure how 'headlandheightforturn' should be set, so for now, only set to '0' or nothing
        # self.set_int_xml_attribute('headlandheightforturn', 0, set_cond=self.headland_height_for_turn_is_zero)
        #
        # self.set_float_xml_attribute('radius', self.radius, precision=1, set_cond=(self.radius > 0))

        if final:
            self.xml_modified = False

    def need_save(self) -> bool:
        # check if any waypoint data has changed
        # first, see if the waypoint list length has changed
        if len(self.waypoints) != self.num_waypoints:
            return True

        # now check to see if any waypoints were rearranged or data was modified
        for index, waypoint in enumerate(self.waypoints):
            # Ask waypoint to update xml which will modify 'xml_modified' attribute appropriately.
            waypoint.update_xml(final=False)

            if index+1 != waypoint.num or waypoint.xml_modified:
                return True

        # no save is necessary
        return False

    def save_xml(self, file: Optional[str] = None) -> None:
        if not file:
            file = self.file

        if file:
            # update waypoint numbers and ask waypoints to update their xml data
            for index, waypoint in enumerate(self.waypoints):
                waypoint.num = index + 1
                waypoint.update_xml()

            # update waypoint length
            self.num_waypoints = len(self.waypoints)

            self.xml_tree.write(file)


class CourseFolder:
    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, value: NumStrType) -> None:
        try:
            value = int(value)
        except ValueError:
            pass
        else:
            if value >= 0:
                self.__id = value

    @property
    def parent(self) -> int:
        return self.__parent

    @parent.setter
    def parent(self, value: NumStrType) -> None:
        try:
            value = int(value)
        except ValueError:
            pass
        else:
            if value >= 0:
                self.__parent = value

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, value: str) -> None:
        self.__name = value

    def __init__(self, folder: ET.Element):
        self.__folder = folder

        self.id = self.get_xml_attribute('id')
        self.parent = self.get_xml_attribute('parent')
        self.name = self.get_xml_attribute('name')

        self.xml_modified = False

    def get_xml_attribute(self, attrib: str) -> Any:
        if attrib == 'id':
            return self.__folder.get('id', 0)
        elif attrib == 'parent':
            return self.__folder.get('parent', 0)
        elif attrib == 'name':
            return self.__folder.get('name', default=f'NO_NAME{self.id}')

        return None

    def update_xml(self, final: bool = True) -> None:
        # Required:
        #   id:       int  = '{id:d}'
        #   parent:   int  = '{parent:d}'
        #   name:     str  = name

        # self.set_int_bool_xml_attribute('wait', self.wait)
        # self.set_int_bool_xml_attribute('unload', self.unload)
        # self.set_int_bool_xml_attribute('rev', self.reverse)
        # self.set_int_bool_xml_attribute('crossing', self.crossing)
        # self.set_int_bool_xml_attribute('turnstart', self.turn_start)
        # self.set_int_bool_xml_attribute('turnend', self.turn_end)
        #
        # self.set_bool_xml_attribute('generated', self.generated)
        # self.set_bool_xml_attribute('isconnectingtrack', self.is_connecting_track)
        #
        # self.set_int_xml_attribute('lane', self.lane, set_cond=(self.lane < 0))
        # self.set_int_xml_attribute('ridgemarker', self.ridge_marker, set_cond=(self.ridge_marker != 0))
        #
        # # Not sure how 'headlandheightforturn' should be set, so for now, only set to '0' or nothing
        # self.set_int_xml_attribute('headlandheightforturn', 0, set_cond=self.headland_height_for_turn_is_zero)
        #
        # self.set_float_xml_attribute('radius', self.radius, precision=1, set_cond=(self.radius > 0))

        if final:
            self.xml_modified = False


class CourseManager:
    def __init__(self, file: str):
        self.file: str = file
        self.xml_tree: ET.ElementTree = ET.parse(file)
        self.root: ET.Element = self.xml_tree.getroot()

        self.__next_unused_id: int = 0

        # root should be tagged with courseManager
        if self.root.tag != 'courseManager':
            raise RuntimeError(f'{file} is not a valid courseManager file.')

        # create Course objects and save in a dict with id's as keys
        self.courses: Dict[int, Course] = {}

        for slot in self.root.iterfind('saveSlot/slot'):
            course = Course(slot, coursemanager=self)
            self.courses[course.id] = course

        # create Folder objects and save in a dict with id's as keys
        self.folders: Dict[int, CourseFolder] = {}

        for folder in self.root.iterfind('folders/folder'):
            course_folder = CourseFolder(folder)
            self.folders[course_folder.id] = course_folder

    def get_unique_id(self) -> int:
        next_id = self.__next_unused_id
        self.__next_unused_id += 1

        return next_id

    def need_save(self) -> bool:
        # check if any folder items have changed
        for folder_id, folder in self.folders.items():
            # Ask folder to update its xml data which will modify 'xml_modified' appropriately.
            folder.update_xml(final=False)

            if folder.xml_modified:
                return True

        # check if any course items have changed
        for course_id, course in self.courses.items():
            # Ask course to update its xml data which will modify 'xml_modified' appropriately.
            course.update_xml(final=False)

            if course.xml_modified:
                return True

        return False

    def save_xml(self, file: Optional[str] = None) -> None:
        if not file:
            file = self.file

        if file:
            for folder_id, folder in self.folders.items():
                folder.update_xml()

            for course_id, course in self.courses.items():
                course.update_xml()

            self.xml_tree.write(file)

    def save(self) -> None:
        # check if saving is necessary
        need_save = False

        if self.need_save():
            need_save = True

        if not need_save:
            for course_id, course in self.courses.items():
                if course.need_save():
                    need_save = True
                    break

        if need_save:
            print('saving...')

            # start by making backups into a directory named 'backups'
            backup_dir = pathlib.Path(pathlib.PurePath(self.file).parent, 'backups')

            # check if backup directory exists
            if not backup_dir.exists():
                backup_dir.mkdir()

            # 'backups' exists but is not a directory
            if not backup_dir.is_dir():
                raise RuntimeError('Backup directory already exists and is not a directory.')

            # find all folders in backups that matches 'backup_datetime_number'
            for num in range(MAXBACKUPS, 0, -1):
                backup_paths = backup_dir.glob(f'backup_*_{num}')

                path_gen = (path for path in backup_paths if path.is_dir())

                for path in path_gen:
                    backup_name, backup_num = path.name.rsplit('_', 1)

                    if int(backup_num) < MAXBACKUPS:
                        path.rename(pathlib.PurePath(path.parent, f'{backup_name}_{int(backup_num) + 1}'))
                    else:
                        shutil.rmtree(path)

            new_backup_dir = pathlib.Path(backup_dir, f"backup_{time.strftime('%Y%m%d%H%M%S')}_1")
            new_backup_dir.mkdir()

            # copy then save coursemanager file if necessary
            if self.need_save():
                shutil.copy2(self.file, new_backup_dir, follow_symlinks=True)
                self.save_xml()

            # copy then save all course files if necessary
            for course_id, course in self.courses.items():
                if course.need_save():
                    shutil.copy2(course.file, new_backup_dir, follow_symlinks=True)
                    course.save_xml()
